const app = new Vue({
  el: '#app',
  data: {
    error: null,
    countryName: null,
    countries: null,
    loading: false,
  },
  methods: {
    submitForm: function (e) {
      e.preventDefault();
      this.error = this.countryName ? null : 'Por favor, complete el siguiente campo para continuar';
      const url = "http://localhost:8000/api/countries?q=" + this.countryName;
      if (!this.error) {
        this.loading = true;
        this.$http.get(url)
          .then((data) => {
            this.loading = false;
            this.countries = data.body
          });
      }
    }
  }
});


